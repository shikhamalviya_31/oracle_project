package stepdefinitions;

import cucumber.api.java.en.When;
import pages.Searchandstorekeywords;

public class WhenSteps {

	Searchandstorekeywords searchnstorekeywords= new Searchandstorekeywords();
	
	@When("^user enters \"([^\"]*)\" and clicks search$")
	public void clickbtn(String keyword) throws InterruptedException {
		searchnstorekeywords.clicksearchbtn(keyword);
	}
}
