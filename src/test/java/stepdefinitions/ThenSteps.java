package stepdefinitions;

import java.io.File;

import cucumber.api.java.en.Then;
import pages.Searchandstorekeywords;

public class ThenSteps {
	Searchandstorekeywords searchnstorekeywords= new Searchandstorekeywords();
	File file;
	
	@Then("^the search should be displayed with results and user stores \"([^\"]*)\" results in a file$")
	public void verifysearchandstoreinfile(int numberofresults) {
		file=searchnstorekeywords.searchresultandstoreinfile(numberofresults);
	}
	
	@Then("^the user should be able to display in console reading from file$")
	public void displayresultfromfile() {
		searchnstorekeywords.ReadfromFile(file);
	}
	
	@Then("^user should be able to open top \"([^\"]*)\" results in a page and validate the link is not broken$")
	public void fnvalidatelink(int numberofresult) {
		searchnstorekeywords.validatebrokenlinksinGooglepagesearchresults(numberofresult);
	}
	
	@Then("^number of occurences of \"([^\"]*)\" should be displayed in console for \"([^\"]*)\" results$")
	public void fnvalidatelink(String keyword,int number) throws InterruptedException {
		searchnstorekeywords.fnFindnumberofoccurencesinPage(keyword, number);
	}
}
