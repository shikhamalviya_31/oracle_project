package pages;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common.Utility;

public class Searchandstorekeywords {

	protected static Utility utils=new Utility();
	public static WebDriver driver;
	public static Properties prop,prop1;
	WebDriverWait wait;
	
	public Searchandstorekeywords() {
	String sPath = System.getProperty("user.dir");
	sPath= sPath+File.separator+"properities"+File.separator+"config.properities";
	prop=utils.loadconfig(sPath);
	
	String sPath1 = System.getProperty("user.dir");
	sPath= sPath1+File.separator+"properities"+File.separator+"Searchandstorekeywords.properities";
	prop1=utils.loadconfig(sPath1);
	wait= new WebDriverWait(driver,15);
	}
	
	public void navigateURL(String sUrl){
		driver=utils.initdriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get(prop.getProperty("URL"));
	}
	
	public void clicksearchbtn(String searchword) throws InterruptedException {
		
		
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys(searchword);
		Thread.sleep(20);
		WebElement matchingResult= driver.findElement(By.xpath(prop1.getProperty("XPATH_GOOGLE_SEARCH_INPUT")));
		List<WebElement> listResult= matchingResult.findElements(By.xpath(prop1.getProperty("XPATH_GOOGLE_SEARCHMATCHLIST")));
		System.out.println(listResult.size());
		//if you want to print matching results
		     for(WebElement results: listResult)
		       {
		         String value= results.getText();
		       if(value.equalsIgnoreCase(searchword)) {
		    	   results.click();
		       }
		       } 
		    
		     
	}
	
	public void WriteToFile(File file,String data){
		 
		    try {
		    	
		      FileWriter myWriter = new FileWriter(file);
		      myWriter.write(data);
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		   
		  }
		}
	
	
	public void fnwindowHandle() {
		Set<String> winIds= driver.getWindowHandles();
		Iterator<String> itr=winIds.iterator();
		String mainwindowId=itr.next();
		for(String currentwindow:winIds) {
			if(!currentwindow.equals(mainwindowId)) {
				driver.switchTo().window(currentwindow);
				driver.manage().window().maximize();
			}
			
		}
		
	}
	
	public File searchresultandstoreinfile(int numberofresult) {
		 File file= new File(".//target/searchlistfile.txt");
		 int counter=0;
		
		if(driver.findElement(By.xpath(prop1.getProperty("XPATH_GOOGLE_SEARCHLIST"))).isDisplayed()){
			System.out.println("Search results displayed");
		 List<WebElement> searchlist = driver.findElements(By.xpath(prop1.getProperty("XPATH_GOOGLE_SEARCHLIST")));
		
		 for(WebElement result: searchlist)
	       {
			 counter =counter+1;
			 if(counter<=numberofresult) {
	         String value= result.getText();
	        WriteToFile(file,value+"//n");
			 } else {
				 break;
			 }
		}
	}else {
		System.out.println("Search results are not displayed");
		
	}
		return file;
}
	 public void ReadfromFile(File file) {
		 try {
		    
		      Scanner myReader = new Scanner(file);
		      while (myReader.hasNextLine()) {
		        String data = myReader.nextLine();
		        System.out.println(data);
		      }
		      myReader.close();
		    } catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	 }
	 
	 
	 public void validatebrokenlinksinGooglepagesearchresults(int numberofresult) {
		 int counter=0;
		
		 String url = "";
	        HttpURLConnection huc = null;
	        int respCode = 200;
		 if(driver.findElement(By.xpath(prop1.getProperty("XPATH_GOOGLE_SEARCHLIST"))).isDisplayed()){
				System.out.println("Search results displayed");
			 List<WebElement> links = driver.findElements(By.xpath(prop1.getProperty("XPATH_GOOLE_SEARCHLIST_LINKS")));
			
			
				 
				 if(counter<=numberofresult+1) {
					 counter =counter+1;
		        
		         Iterator<WebElement> it = links.iterator();
		         
		         while(it.hasNext()){
		             
		             url = it.next().getAttribute("href");
		             
		             System.out.println(url);
		         
		             if(url == null || url.isEmpty()){
		 System.out.println("URL is either not configured for anchor tag or it is empty");
		                 continue;
		             }
		             
		            
		         try {
		                huc = (HttpURLConnection)(new URL(url).openConnection());
		                
		                huc.setRequestMethod("HEAD");
		                
		                huc.connect();
		                
		                respCode = huc.getResponseCode();
		                
		                if(respCode >= 400){
		                    System.out.println(url+" is a broken link");
		                }
		                else{
		                    System.out.println(url+" is a valid link");
		                }
		                    
		            } catch (MalformedURLException e) {
		                // TODO Auto-generated catch block
		                e.printStackTrace();
		            } catch (IOException e) {
		                // TODO Auto-generated catch block
		                e.printStackTrace();
		            }
		        }
		         
				 } 
			}
		else {
			System.out.println("Search results are not displayed");
			
		}
	 }
	 
	 public void fnFindnumberofoccurencesinPage(String keyword,int numberofresult) throws InterruptedException {
		 
			int counter=0;	
		    int respCode = 200;
		    int count=0;
			 if(driver.findElement(By.xpath("//div[@id='rso']//div[@class='g']//a/h3")).isDisplayed()){
					System.out.println("Search results displayed");
				 List<WebElement> links = driver.findElements(By.xpath(prop1.getProperty("XPATH_GOOLE_SEARCHLIST_LINKS")));
				
			
					 
					 for(WebElement result: links)
				       {
						 counter =counter+1;
						 if(counter<=numberofresult) {
				         String value= result.getText();
				         
				       result.click();
				       Thread.sleep(1000);
				       String src=driver.getPageSource();
				       String[] srckey=src.split(" ");
				      for(String s:srckey)
				       {
				    	  if(s.equalsIgnoreCase(keyword)) {
				    		  count=count+1;
				    	  }
				       }
						 } else {
							 break;
						 }
					}
			System.out.println("Number of occurences of "+keyword+"is: "+count);
				driver.navigate().back();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOfAllElements(links));
		
		 
	 }
	 }
}
