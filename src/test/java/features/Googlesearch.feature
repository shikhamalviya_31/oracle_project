Feature: Google search and save in file

  Scenario Outline: Display google search results in console by reading from file
    Given user navigate to "google search"
    When user enters "<Keyword>" and clicks search
    Then the search should be displayed with results and user stores "10" results in a file
    And the user should be able to display in console reading from file
    
    Examples:
    |Keyword|
    |Automation|
    |Testing|
    |API|
    |KARNATAKA|
    |Performance Testing|
    
    
    Scenario Outline: 2 Check all links after google search and display number of occurrences of the keyword
    Given user navigate to "google search"
    When user enters "<Keyword>" and clicks search
    Then user should be able to open top "10" results in a page and validate the link is not broken
    And number of occurences of "<Keyword>" should be displayed in console for "10" results
    Examples:
    |Keyword|
    |Automation|
    
