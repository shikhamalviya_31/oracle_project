package Common;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
public class Utility {
 
	public static WebDriver driver;
	
	public Properties loadconfig(String sPath) {
		Properties prop=new Properties();
		try {
			FileInputStream fs = new FileInputStream(sPath);
			prop.load(fs);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	public static WebDriver initdriver() {
		System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"//repositories//geckodriver.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette",true);
		driver= new FirefoxDriver(capabilities);
		return driver;
	}
}
