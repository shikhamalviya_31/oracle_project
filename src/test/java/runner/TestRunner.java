package runner;

import org.junit.runner.RunWith;
import org.junit.runner.Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "src/test/java/features",
glue= {"src/test/java/stepdefinitions/"},
format= {"pretty","html:target/cucumber-pretty-html-report","rerun:target/rerun.txt"},
monochrome= true)

public class TestRunner {

}
